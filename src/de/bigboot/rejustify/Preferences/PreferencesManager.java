package de.bigboot.rejustify.Preferences;

import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;

/**
 * Encrypted Preferences 
 * Source: http://www.drdobbs.com/security/encrypted-preferences-in-java/184416587
 */
public class PreferencesManager {
	private HashMap<String, String> settings = new HashMap<String, String>();
	private Preferences preferences;

	public PreferencesManager() {
		String key = "O39bH6Or6PsG0jcBmZssuNOVys2W665s";
		DESKeySpec dks;
		try {
			dks = new DESKeySpec(key.getBytes());
			SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
			SecretKey secretKey = keyFactory.generateSecret(dks);

			preferences = EncryptedPreferences.userNodeForPackage(
					PreferencesManager.class, secretKey);
//			preferences = Preferences.userNodeForPackage(PreferencesManager.class);

			for (Entry<String, String> entry : settings.entrySet()) {
				preferences.put(entry.getKey(), entry.getValue());
			}
		} catch (InvalidKeyException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvalidKeySpecException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void load() throws BackingStoreException {
		settings.clear();
//		for (String key : preferences.keys()) {
//			settings.put(key, preferences.get(key, null));
//		}
	}

	public void save() {
		for (Entry<String, String> entry : settings.entrySet()) {
			if(entry.getKey()==null||entry.getValue()==null)
			{
				continue;
			}
			preferences.put(entry.getKey(), entry.getValue());
		}
		try {
			preferences.sync();
		} catch (BackingStoreException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public void setValue(String key, String value) {
		settings.put(key, value);
	}

	public String getValue(String key) {
		if(!settings.containsKey(key))
		{
			settings.put(key, preferences.get(key, null));
		}
		return settings.get(key);
	}
}
