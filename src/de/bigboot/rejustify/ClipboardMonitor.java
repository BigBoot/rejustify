/*
    This file is part of ReJustify.

    ReJustify is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ReJustify is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ReJustify.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.bigboot.rejustify;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.SwingWorker;

/**
 * Monitors the clipboard for changes. Use with {@link ClipboardListener}
 * 
 */
class ClipboardMonitor {
	private SwingWorker<Void, String> worker;
	private String clipboardContent;
	private ArrayList<ClipboardListener> listeners = new ArrayList<ClipboardListener>();

	/**
	 * Add {@link ClipboardListener}
	 * 
	 * @param listener
	 *            the {@link ClipboardListener} to be added
	 */
	public void addListener(ClipboardListener listener) {
		listeners.add(listener);
	}

	/**
	 * Remove {@link ClipboardListener}
	 * 
	 * @param listener
	 *            the {@link ClipboardListener} to be removed
	 */
	public void removeListener(ClipboardListener listener) {
		listeners.remove(listener);
	}

	/**
	 * Default Constructor Starts monitoring the clipboard
	 */
	public ClipboardMonitor() {
		start();
	}

	/**
	 * Get the String residing on the clipboard.
	 * 
	 * @return any text found on the Clipboard; if none found, return an empty
	 *         String.
	 */
	public String getClipboardContents() {
		String result = "";
		Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
		// odd: the Object param of getContents is not currently used
		Transferable contents = clipboard.getContents(null);
		boolean hasTransferableText = (contents != null)
				&& contents.isDataFlavorSupported(DataFlavor.stringFlavor);
		if (hasTransferableText) {
			try {
				result = (String) contents
						.getTransferData(DataFlavor.stringFlavor);
			} catch (UnsupportedFlavorException ex) {
				// highly unlikely since we are using a standard DataFlavor
				System.out.println(ex);
				ex.printStackTrace();
			} catch (IOException ex) {
				System.out.println(ex);
				ex.printStackTrace();
			}
		}
		return result;
	}

	/**
	 * Start monitoring the clipboard
	 */
	public void start() {
		if (worker != null) {
			return;
		}
		worker = new SwingWorker<Void, String>() {
			@Override
			protected Void doInBackground() throws Exception {
				clipboardContent = getClipboardContents();
				while (!isCancelled()) {
					String newContent = getClipboardContents();
					if (!newContent.equals(clipboardContent)) {
						publish(newContent);
						clipboardContent = newContent;
					}
					Thread.sleep(500);
				}
				return null;
			}

			@Override
			protected void process(List<String> chunks) {
				for (String s : chunks) {
					for (ClipboardListener listener : listeners) {
						if (listener == null) {
							continue;
						}
						listener.clipboardChanged(s);
					}
				}
			}
		};
		worker.execute();
	}

	/**
	 * Stop monitoring the clipboard
	 */
	public void stop() {
		if (worker == null) {
			return;
		}
		worker.cancel(true);
		worker = null;
	}

}