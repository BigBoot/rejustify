package de.bigboot.rejustify;

import java.text.DecimalFormat;

public abstract class Utils {
	public static String readableFileSize(long size) {
	    if(size <= 0) return "0 B/s";
	    final String[] units = new String[] { "B/s", "KB/s", "MB/s", "GB/s", "TB/s" };
	    int digitGroups = (int) (Math.log10(size)/Math.log10(1024));
	    return new DecimalFormat("#,##0.#").format(size/Math.pow(1024, digitGroups)) + " " + units[digitGroups];
	}
}
