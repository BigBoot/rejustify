/*
    This file is part of ReJustify.

    ReJustify is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ReJustify is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ReJustify.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.bigboot.rejustify;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTabbedPane;
import java.awt.BorderLayout;
import javax.swing.JPanel;
import java.awt.GridBagLayout;
import javax.swing.JButton;
import java.awt.GridBagConstraints;
import javax.swing.JTextField;

import java.awt.Desktop;
import java.awt.Insets;
import java.awt.FlowLayout;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.HashMap;
import java.util.prefs.BackingStoreException;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JTextArea;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.Timer;
import javax.swing.table.DefaultTableModel;
import javax.swing.JScrollPane;

import de.bigboot.rejustify.Preferences.PreferencesManager;
import de.felixbruns.jotify.exceptions.ConnectionException;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.JLabel;
import javax.swing.border.TitledBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JPasswordField;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import javax.swing.JCheckBox;
import javax.swing.UIManager;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.Color;
import java.awt.Dimension;

/**
 * Main ReJustify class Containing the GUI
 */
public class ReJustify implements ReJustifyListener {
	private static final int COLUMN_TRACK = 0;
	private static final int COLUMN_ARTIST = 1;
	private static final int COLUMN_ALBUM = 2;
	private static final int COLUMN_TIME = 3;
	private static final int COLUMN_STATUS = 4;
	private static final int COLUMN_PROGRESS = 5;
	private static final int COLUMN_ID = 6;

	private JFrame frmRejustify;
	private Justifyx justifyx;
	private DefaultTableModel TableModel;
	private JTable table;
	private JTextField textField;
	private boolean downloading = false;
	private boolean paused = true;
	private HashMap<String, Track> tracks = new HashMap<String, Track>();
	private JButton btnStart;
	private JButton btnStop;
	private JTextField txtOut;
	private JTextField txtUser;
	private JTextField txtName;
	private JPasswordField txtPass;
	private ClipboardMonitor clipboardMonitor;
	private PreferencesManager preferences;
	private JCheckBox chckbxMonitorClipboard;
	private JComboBox<String> comboQualtity;
	private SpeedGraph speedGraph;
	private JCheckBox chckbxSkipExisingTracks;
	private JComboBox<String> comboCreatePlaylistFolder;
	private JTabbedPane tabbedPane;
	private JTextArea txtLog;
	private JScrollPane scrollPane;
	private JCheckBox chckbxShowLog;
	private OutputStream outStream;
	private JLabel lblNewLabel_1;
	private JButton btnOpenDownloadFolder;

	/**
	 * Start Downloading
	 */
	public void start() {
		if (downloading || paused) {
			return;
		}
		btnStart.setText("Pause");
		downloading = true;
		int i = getNextDownload();
		if (i < 0) {
			downloading = false;
			paused = true;
			btnStart.setText("Start");
			return;
		}
		justifyx.downloadTrack(tracks.get(TableModel.getValueAt(i, COLUMN_ID)));
	}

	/**
	 * Get the index of the next track to be downloaded in the table
	 * 
	 * @return index of track
	 */
	public int getNextDownload() {
		for (int i = 0; i < TableModel.getRowCount(); i++) {
			if (TableModel.getValueAt(i, COLUMN_STATUS).equals("Queued")) {
				return i;
			}
		}
		return -1;
	}

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			UIManager
					.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
		} catch (Throwable e) {
			e.printStackTrace();
		}
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ReJustify window = new ReJustify();
					window.frmRejustify.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public ReJustify() {
		outStream = System.out;
		initialize();
		clipboardMonitor = new ClipboardMonitor();
		redirectSystemStreams();
		loadSettings();
		new Timer(1000, new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				long speed = justifyx.getSpeed();
				String s = Utils.readableFileSize(speed);
				frmRejustify.setTitle("ReJustify - " + s);
				speedGraph.setCurrentSpeed(speed);
			}
		}).start();
	}

	/**
	 * load saved settings
	 */
	private void loadSettings() {
		String user = getSetting(PreferenceKeys.USERNAME);
		String pass = getSetting(PreferenceKeys.PASSWORD);
		if (user != "" && pass != "") {
			justifyx.loginUser(user, pass);
		}
		boolean monitor = getSetting(PreferenceKeys.MONITOR_CLIPBOARD).equals(
				"true");
		chckbxMonitorClipboard.setSelected(monitor);
		if (monitor) {
			clipboardMonitor = new ClipboardMonitor();
			clipboardMonitor.addListener(new ClipboardListener() {
				@Override
				public void clipboardChanged(String value) {
					justifyx.getTracks(value);
				}
			});
		}
		boolean skipExisitng = getSetting(PreferenceKeys.SKIP_EXISTING).equals(
				"true");
		justifyx.setSkipExisitingTracks(skipExisitng);
		chckbxSkipExisingTracks.setSelected(skipExisitng);
		String createFolders = getSetting(PreferenceKeys.CREATE_FOLDER);
		if (createFolders.equals("true")) {
			comboCreatePlaylistFolder.setSelectedIndex(0);
			justifyx.setCreatePlaylistFolder(false);
			justifyx.setNoFolders(false);
		} else if (createFolders.equals("playlist")) {
			comboCreatePlaylistFolder.setSelectedIndex(1);
			justifyx.setCreatePlaylistFolder(true);
			justifyx.setNoFolders(false);
		} else {
			comboCreatePlaylistFolder.setSelectedIndex(2);
			justifyx.setCreatePlaylistFolder(false);
			justifyx.setNoFolders(true);
		}
		boolean showLog = getSetting(PreferenceKeys.SHOW_LOG).equals("true");
		chckbxShowLog.setSelected(showLog);
		setLogVisible(showLog);
		String path = getSetting(PreferenceKeys.OUTPUT_FOLDER);
		txtOut.setText(path);
		justifyx.setOutputFolder(path);
		String format = getSetting(PreferenceKeys.OUTPUT_FORMAT);
		if (format.equals("ogg_96")) {
			comboQualtity.setSelectedIndex(2);
		} else if (format.equals("ogg_160")) {
			comboQualtity.setSelectedIndex(1);
		} else if (format.equals("ogg_320")) {
			comboQualtity.setSelectedIndex(0);
		}
		justifyx.setFormat(format);
		String naming = getSetting(PreferenceKeys.NAMING);
		txtName.setText(naming);
		justifyx.setPattern(naming);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		preferences = new PreferencesManager();
		try {
			preferences.load();
		} catch (BackingStoreException e2) {
			e2.printStackTrace();
		}
		frmRejustify = new JFrame();
		frmRejustify.setTitle("ReJustify - 0 B/s");
		frmRejustify.setBounds(100, 100, 600, 471);
		frmRejustify.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmRejustify.addWindowListener(new WindowAdapter() {

			@Override
			public void windowClosing(WindowEvent e) {
				try {
					justifyx.close();
					preferences.save();
				} catch (ConnectionException e1) {
					e1.printStackTrace();
				}
			}

		});

		tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		frmRejustify.getContentPane().add(tabbedPane, BorderLayout.CENTER);

		JPanel panel = new JPanel();
		tabbedPane.addTab("Download", null, panel, null);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[] { 0, 0, 0 };
		gbl_panel.rowHeights = new int[] { 0, 0, 0 };
		gbl_panel.columnWeights = new double[] { 1.0, 0.0, Double.MIN_VALUE };
		gbl_panel.rowWeights = new double[] { 0.0, 1.0, Double.MIN_VALUE };
		panel.setLayout(gbl_panel);

		textField = new JTextField();
		textField.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				justifyx.getTracks(textField.getText());
				textField.setText("");
			}
		});
		GridBagConstraints gbc_textField = new GridBagConstraints();
		gbc_textField.insets = new Insets(0, 0, 5, 5);
		gbc_textField.fill = GridBagConstraints.HORIZONTAL;
		gbc_textField.gridx = 0;
		gbc_textField.gridy = 0;
		panel.add(textField, gbc_textField);
		textField.setColumns(10);

		JButton btnAdd = new JButton("Add");
		btnAdd.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				justifyx.getTracks(textField.getText());
				textField.setText("");
			}
		});
		GridBagConstraints gbc_btnAdd = new GridBagConstraints();
		gbc_btnAdd.insets = new Insets(0, 0, 5, 0);
		gbc_btnAdd.gridx = 1;
		gbc_btnAdd.gridy = 0;
		panel.add(btnAdd, gbc_btnAdd);

		JScrollPane scrollPane_1 = new JScrollPane();
		GridBagConstraints gbc_scrollPane_1 = new GridBagConstraints();
		gbc_scrollPane_1.gridwidth = 2;
		gbc_scrollPane_1.fill = GridBagConstraints.BOTH;
		gbc_scrollPane_1.gridx = 0;
		gbc_scrollPane_1.gridy = 1;
		panel.add(scrollPane_1, gbc_scrollPane_1);

		TableModel = new DefaultTableModel(new Object[][] {},
				new String[] { "Track", "Artist", "Album", "Time", "Status",
						"Progress", "ID" });
		table = new JTable(TableModel) {
			private static final long serialVersionUID = 8463959069923285372L;

			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}

		};
		table.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_DELETE) {
					deleteSelectedRows();
				}
			}
		});
		table.removeColumn(table.getColumnModel().getColumn(COLUMN_ID));
		table.getColumnModel().getColumn(COLUMN_PROGRESS)
				.setCellRenderer(new ProgressCellRenderer());
		table.setShowVerticalLines(false);
		table.setDragEnabled(true);
		scrollPane_1.setViewportView(table);

		JPanel panel_1 = new JPanel();
		tabbedPane.addTab("Settings", null, panel_1, null);
		GridBagLayout gbl_panel_1 = new GridBagLayout();
		gbl_panel_1.columnWidths = new int[] { 0, 0, 0, 0, 0 };
		gbl_panel_1.rowHeights = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0 };
		gbl_panel_1.columnWeights = new double[] { 0.0, 1.0, 1.0, 0.0,
				Double.MIN_VALUE };
		gbl_panel_1.rowWeights = new double[] { 0.0, 0.0, 0.0, 0.0, 0.0, 0.0,
				1.0, 0.0, Double.MIN_VALUE };
		panel_1.setLayout(gbl_panel_1);

		JPanel panel_5 = new JPanel();
		panel_5.setBorder(new TitledBorder(null, "Spotify",
				TitledBorder.LEADING, TitledBorder.TOP, null, null));
		GridBagConstraints gbc_panel_5 = new GridBagConstraints();
		gbc_panel_5.insets = new Insets(0, 0, 5, 0);
		gbc_panel_5.gridwidth = 4;
		gbc_panel_5.fill = GridBagConstraints.BOTH;
		gbc_panel_5.gridx = 0;
		gbc_panel_5.gridy = 0;
		panel_1.add(panel_5, gbc_panel_5);
		GridBagLayout gbl_panel_5 = new GridBagLayout();
		gbl_panel_5.columnWidths = new int[] { 0, 0, 0, 0, 0, 0 };
		gbl_panel_5.rowHeights = new int[] { 15, 0 };
		gbl_panel_5.columnWeights = new double[] { 0.0, 1.0, 0.0, 1.0, 0.0,
				Double.MIN_VALUE };
		gbl_panel_5.rowWeights = new double[] { 0.0, Double.MIN_VALUE };
		panel_5.setLayout(gbl_panel_5);

		JLabel lblUsername = new JLabel("Username:");
		GridBagConstraints gbc_lblUsername = new GridBagConstraints();
		gbc_lblUsername.insets = new Insets(0, 0, 0, 5);
		gbc_lblUsername.anchor = GridBagConstraints.EAST;
		gbc_lblUsername.gridx = 0;
		gbc_lblUsername.gridy = 0;
		panel_5.add(lblUsername, gbc_lblUsername);

		txtUser = new JTextField(getSetting(PreferenceKeys.USERNAME));
		GridBagConstraints gbc_txtUser = new GridBagConstraints();
		gbc_txtUser.insets = new Insets(0, 0, 0, 5);
		gbc_txtUser.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtUser.gridx = 1;
		gbc_txtUser.gridy = 0;
		panel_5.add(txtUser, gbc_txtUser);
		txtUser.setColumns(10);

		JLabel lblPassword = new JLabel("Password:");
		GridBagConstraints gbc_lblPassword = new GridBagConstraints();
		gbc_lblPassword.anchor = GridBagConstraints.EAST;
		gbc_lblPassword.insets = new Insets(0, 0, 0, 5);
		gbc_lblPassword.gridx = 2;
		gbc_lblPassword.gridy = 0;
		panel_5.add(lblPassword, gbc_lblPassword);

		JButton btnLogIn = new JButton("Log in");
		btnLogIn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				login();
			}
		});

		txtPass = new JPasswordField(getSetting(PreferenceKeys.PASSWORD));
		txtPass.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				login();
			}
		});
		GridBagConstraints gbc_txtPass = new GridBagConstraints();
		gbc_txtPass.insets = new Insets(0, 0, 0, 5);
		gbc_txtPass.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtPass.gridx = 3;
		gbc_txtPass.gridy = 0;
		panel_5.add(txtPass, gbc_txtPass);
		GridBagConstraints gbc_btnLogIn = new GridBagConstraints();
		gbc_btnLogIn.gridx = 4;
		gbc_btnLogIn.gridy = 0;
		panel_5.add(btnLogIn, gbc_btnLogIn);

		JLabel lblQuality = new JLabel("Quality:");
		GridBagConstraints gbc_lblQuality = new GridBagConstraints();
		gbc_lblQuality.anchor = GridBagConstraints.WEST;
		gbc_lblQuality.insets = new Insets(0, 0, 5, 5);
		gbc_lblQuality.gridx = 0;
		gbc_lblQuality.gridy = 1;
		panel_1.add(lblQuality, gbc_lblQuality);

		comboQualtity = new JComboBox<String>();
		comboQualtity.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {
					String format = "";
					if (e.getItem().equals("320kbps")) {
						format = "ogg_320";
					} else if (e.getItem().equals("160kbps")) {
						format = "ogg_160";
					} else if (e.getItem().equals("96kbps")) {
						format = "ogg_96";
					}
					justifyx.setFormat(format);
					setSetting(PreferenceKeys.OUTPUT_FORMAT, format);
				}
			}
		});
		comboQualtity.setModel(new DefaultComboBoxModel<String>(new String[] {
				"320kbps", "160kbps", "96kbps" }));
		GridBagConstraints gbc_comboQualtity = new GridBagConstraints();
		gbc_comboQualtity.insets = new Insets(0, 0, 5, 0);
		gbc_comboQualtity.gridwidth = 3;
		gbc_comboQualtity.fill = GridBagConstraints.HORIZONTAL;
		gbc_comboQualtity.gridx = 1;
		gbc_comboQualtity.gridy = 1;
		panel_1.add(comboQualtity, gbc_comboQualtity);

		JPanel panel_6 = new JPanel();
		panel_6.setBorder(new TitledBorder(null, "Output",
				TitledBorder.LEADING, TitledBorder.TOP, null, null));
		GridBagConstraints gbc_panel_6 = new GridBagConstraints();
		gbc_panel_6.insets = new Insets(0, 0, 5, 0);
		gbc_panel_6.gridwidth = 4;
		gbc_panel_6.fill = GridBagConstraints.BOTH;
		gbc_panel_6.gridx = 0;
		gbc_panel_6.gridy = 2;
		panel_1.add(panel_6, gbc_panel_6);
		GridBagLayout gbl_panel_6 = new GridBagLayout();
		gbl_panel_6.columnWidths = new int[] { 0, 0, 0, 0, 0, 0 };
		gbl_panel_6.rowHeights = new int[] { 0, 0, 0, 0 };
		gbl_panel_6.columnWeights = new double[] { 0.0, 0.0, 1.0, 0.0, 0.0,
				Double.MIN_VALUE };
		gbl_panel_6.rowWeights = new double[] { 0.0, 0.0, 0.0, Double.MIN_VALUE };
		panel_6.setLayout(gbl_panel_6);

		JLabel lblOutputFolder = new JLabel("Output Folder:");
		GridBagConstraints gbc_lblOutputFolder = new GridBagConstraints();
		gbc_lblOutputFolder.anchor = GridBagConstraints.WEST;
		gbc_lblOutputFolder.gridwidth = 2;
		gbc_lblOutputFolder.insets = new Insets(0, 0, 5, 5);
		gbc_lblOutputFolder.gridx = 0;
		gbc_lblOutputFolder.gridy = 0;
		panel_6.add(lblOutputFolder, gbc_lblOutputFolder);

		txtOut = new JTextField(new File("").getAbsolutePath());
		txtOut.setEditable(false);
		GridBagConstraints gbc_txtOut = new GridBagConstraints();
		gbc_txtOut.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtOut.insets = new Insets(0, 0, 5, 5);
		gbc_txtOut.gridx = 2;
		gbc_txtOut.gridy = 0;
		panel_6.add(txtOut, gbc_txtOut);
		txtOut.setColumns(10);

		JButton btnOpen = new JButton("...");
		btnOpen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JFileChooser folderBrowser = new JFileChooser(txtOut.getText());
				folderBrowser
						.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
				if (folderBrowser.showOpenDialog(frmRejustify) == JFileChooser.APPROVE_OPTION) {
					txtOut.setText(folderBrowser.getSelectedFile()
							.getAbsolutePath());
					justifyx.setOutputFolder(txtOut.getText());
					setSetting(PreferenceKeys.OUTPUT_FOLDER, txtOut.getText());
				}
			}
		});
		GridBagConstraints gbc_btnOpen = new GridBagConstraints();
		gbc_btnOpen.gridwidth = 2;
		gbc_btnOpen.insets = new Insets(0, 0, 5, 0);
		gbc_btnOpen.gridx = 3;
		gbc_btnOpen.gridy = 0;
		panel_6.add(btnOpen, gbc_btnOpen);

		JLabel lblNewLabel = new JLabel("Pattern:");
		GridBagConstraints gbc_lblNewLabel = new GridBagConstraints();
		gbc_lblNewLabel.gridwidth = 2;
		gbc_lblNewLabel.anchor = GridBagConstraints.WEST;
		gbc_lblNewLabel.insets = new Insets(0, 0, 5, 5);
		gbc_lblNewLabel.gridx = 0;
		gbc_lblNewLabel.gridy = 1;
		panel_6.add(lblNewLabel, gbc_lblNewLabel);

		txtName = new JTextField();
		txtName.getDocument().addDocumentListener(new DocumentListener() {
			@Override
			public void removeUpdate(DocumentEvent e) {
				changedUpdate(e);
			}
			@Override
			public void insertUpdate(DocumentEvent e) {
				changedUpdate(e);
			}
			
			@Override
			public void changedUpdate(DocumentEvent e) {
				String pattern = txtName.getText();
				setSetting(PreferenceKeys.NAMING, pattern);
				justifyx.setPattern(pattern);
			}
		});
		GridBagConstraints gbc_txtName = new GridBagConstraints();
		gbc_txtName.gridwidth = 2;
		gbc_txtName.insets = new Insets(0, 0, 5, 5);
		gbc_txtName.fill = GridBagConstraints.HORIZONTAL;
		gbc_txtName.gridx = 2;
		gbc_txtName.gridy = 1;
		panel_6.add(txtName, gbc_txtName);
		txtName.setColumns(10);
		
		lblNewLabel_1 = new JLabel("?");
		lblNewLabel_1.setToolTipText("<html>\nPossible values are:<br>\n<br>\n%title% - Track title<br>\n%num% - Track number<br>\n%cdnum% - CD number<br>\n%album% - Album title<br>\n%artist% - Artist name<br>\n%playlist% - Playlist name (Only when downloading playlists)<br>\n%duration% - Track duration in format mm_ss<br>\n%spotid%\" - Spotify ID\n</html>");
		GridBagConstraints gbc_lblNewLabel_1 = new GridBagConstraints();
		gbc_lblNewLabel_1.insets = new Insets(0, 0, 5, 0);
		gbc_lblNewLabel_1.gridx = 4;
		gbc_lblNewLabel_1.gridy = 1;
		panel_6.add(lblNewLabel_1, gbc_lblNewLabel_1);

		JLabel lblPlaylist = new JLabel("Folder structure:");
		GridBagConstraints gbc_lblPlaylist = new GridBagConstraints();
		gbc_lblPlaylist.gridwidth = 2;
		gbc_lblPlaylist.anchor = GridBagConstraints.EAST;
		gbc_lblPlaylist.insets = new Insets(0, 0, 0, 5);
		gbc_lblPlaylist.gridx = 0;
		gbc_lblPlaylist.gridy = 2;
		panel_6.add(lblPlaylist, gbc_lblPlaylist);

		comboCreatePlaylistFolder = new JComboBox<String>();
		comboCreatePlaylistFolder.addItemListener(new ItemListener() {
			
			public void itemStateChanged(ItemEvent e) {
				if (e.getStateChange() == ItemEvent.SELECTED) {
					if (e.getItem().equals("Create full folder structure")) {
						justifyx.setCreatePlaylistFolder(false);
						justifyx.setNoFolders(false);
						setSetting(PreferenceKeys.CREATE_FOLDER, "true");
					} else if (e.getItem().equals("Create playlist folder")) {
						justifyx.setCreatePlaylistFolder(true);
						justifyx.setNoFolders(false);
						setSetting(PreferenceKeys.CREATE_FOLDER, "playlist");
					} else if (e.getItem().equals("Create no folders")) {
						justifyx.setCreatePlaylistFolder(true);
						justifyx.setNoFolders(true);
						setSetting(PreferenceKeys.CREATE_FOLDER, "false");
					}
				}
			}
		});
		comboCreatePlaylistFolder.setModel(new DefaultComboBoxModel<String>(
				new String[] { "Create full folder structure",
						"Create playlist folder", "Create no folders" }));
		GridBagConstraints gbc_comboCreatePlaylistFolder = new GridBagConstraints();
		gbc_comboCreatePlaylistFolder.gridwidth = 3;
		gbc_comboCreatePlaylistFolder.insets = new Insets(0, 0, 0, 5);
		gbc_comboCreatePlaylistFolder.fill = GridBagConstraints.HORIZONTAL;
		gbc_comboCreatePlaylistFolder.gridx = 2;
		gbc_comboCreatePlaylistFolder.gridy = 2;
		panel_6.add(comboCreatePlaylistFolder, gbc_comboCreatePlaylistFolder);

		chckbxMonitorClipboard = new JCheckBox("Monitor Clipboard");
		chckbxMonitorClipboard.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JCheckBox box = (JCheckBox) e.getSource();
				if (box.isSelected()) {
					clipboardMonitor.start();
					setSetting(PreferenceKeys.MONITOR_CLIPBOARD, "true");
				} else {
					clipboardMonitor.stop();
					setSetting(PreferenceKeys.MONITOR_CLIPBOARD, "false");
				}
			}
		});
		GridBagConstraints gbc_chckbxMonitorClipboard = new GridBagConstraints();
		gbc_chckbxMonitorClipboard.anchor = GridBagConstraints.WEST;
		gbc_chckbxMonitorClipboard.insets = new Insets(0, 0, 5, 5);
		gbc_chckbxMonitorClipboard.gridx = 0;
		gbc_chckbxMonitorClipboard.gridy = 3;
		panel_1.add(chckbxMonitorClipboard, gbc_chckbxMonitorClipboard);

		JButton btnLicense = new JButton("License");
		btnLicense.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JOptionPane
						.showMessageDialog(
								frmRejustify,
								"ReJustify Spotify Downloader\n"
										+ "Copyright (C) 2013  BigBoot\n"
										+ "\n"
										+ "This program is free software: you can redistribute it and/or modify\n"
										+ "it under the terms of the GNU General Public License as published by\n"
										+ "the Free Software Foundation, either version 3 of the License, or\n"
										+ "(at your option) any later version.\n"
										+ "\n"
										+ "This program is distributed in the hope that it will be useful,\n"
										+ "but WITHOUT ANY WARRANTY; without even the implied warranty of\n"
										+ "MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n"
										+ "GNU General Public License for more details.\n"
										+ "\n"
										+ "You should have received a copy of the GNU General Public License\n"
										+ "along with this program.  If not, see <http://www.gnu.org/licenses/>.");
			}
		});

		chckbxSkipExisingTracks = new JCheckBox("Skip exising Tracks");
		chckbxSkipExisingTracks.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JCheckBox box = (JCheckBox) e.getSource();
				if (box.isSelected()) {
					justifyx.setSkipExisitingTracks(true);
					setSetting(PreferenceKeys.SKIP_EXISTING, "true");
				} else {
					justifyx.setSkipExisitingTracks(false);
					setSetting(PreferenceKeys.SKIP_EXISTING, "false");
				}
			}
		});
		GridBagConstraints gbc_chckbxSkipExisingTracks = new GridBagConstraints();
		gbc_chckbxSkipExisingTracks.anchor = GridBagConstraints.WEST;
		gbc_chckbxSkipExisingTracks.insets = new Insets(0, 0, 5, 5);
		gbc_chckbxSkipExisingTracks.gridx = 0;
		gbc_chckbxSkipExisingTracks.gridy = 4;
		panel_1.add(chckbxSkipExisingTracks, gbc_chckbxSkipExisingTracks);

		chckbxShowLog = new JCheckBox("Show log");
		chckbxShowLog.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JCheckBox box = (JCheckBox) e.getSource();
				if (box.isSelected()) {
					setLogVisible(true);
					setSetting(PreferenceKeys.SHOW_LOG, "true");
				} else {
					setLogVisible(false);
					setSetting(PreferenceKeys.SHOW_LOG, "false");
				}
			}
		});
		GridBagConstraints gbc_chckbxShowLog = new GridBagConstraints();
		gbc_chckbxShowLog.anchor = GridBagConstraints.WEST;
		gbc_chckbxShowLog.insets = new Insets(0, 0, 5, 5);
		gbc_chckbxShowLog.gridx = 0;
		gbc_chckbxShowLog.gridy = 5;
		panel_1.add(chckbxShowLog, gbc_chckbxShowLog);
		GridBagConstraints gbc_btnLicense = new GridBagConstraints();
		gbc_btnLicense.gridx = 3;
		gbc_btnLicense.gridy = 7;
		panel_1.add(btnLicense, gbc_btnLicense);

		scrollPane = new JScrollPane();
		tabbedPane.addTab("Log", null, scrollPane, null);

		txtLog = new JTextArea();
		scrollPane.setViewportView(txtLog);

		JPanel panel_3 = new JPanel();
		frmRejustify.getContentPane().add(panel_3, BorderLayout.NORTH);
		panel_3.setLayout(new BorderLayout(0, 0));

		JPanel panel_2 = new JPanel();
		panel_3.add(panel_2, BorderLayout.WEST);
		panel_2.setLayout(new FlowLayout(FlowLayout.LEFT, 5, 5));

		btnStart = new JButton("Start");
		btnStart.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				JButton btn = (JButton) e.getSource();
				if (btn.getText().equals("Pause")) {
					paused = true;
					btn.setText("Start");
				} else {
					paused = false;
					btn.setText("Pause");
					start();
				}
			}
		});
		panel_2.add(btnStart);

		btnStop = new JButton("Stop");
		btnStop.setVisible(false);
		btnStop.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				paused = true;
				btnStart.setText("Start");
				justifyx.cancelDownload();
			}
		});
		panel_2.add(btnStop);

		JButton btnDelete = new JButton("Delete");
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				deleteSelectedRows();
			}
		});
		panel_2.add(btnDelete);
		
		btnOpenDownloadFolder = new JButton("Open Download Folder");
		btnOpenDownloadFolder.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					Desktop.getDesktop().open(new File(txtOut.getText()));
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		panel_2.add(btnOpenDownloadFolder);

		speedGraph = new SpeedGraph();
		speedGraph.setForeground(Color.GREEN);
		speedGraph.setPreferredSize(new Dimension(200, 0));
		speedGraph.setBackground(Color.WHITE);
		panel_3.add(speedGraph, BorderLayout.EAST);
		justifyx = new Justifyx();
		justifyx.addListener(this);
		justifyx.setOutputFolder(txtOut.getText());
	}

	protected void setLogVisible(boolean b) {
		if (b) {
			tabbedPane.insertTab("Log", null, scrollPane, null, 2);
		} else {
			tabbedPane.removeTabAt(2);
		}
	}

	protected void login() {
		try {
			justifyx.close();
		} catch (ConnectionException e1) {
		}
		char[] pw = txtPass.getPassword();
		if (justifyx.loginUser(txtUser.getText(), new String(pw))) {
			JOptionPane.showMessageDialog(frmRejustify, "Login successfull");
			setSetting(PreferenceKeys.USERNAME, txtUser.getText());
			setSetting(PreferenceKeys.PASSWORD, new String(pw));
		} else {
			JOptionPane.showMessageDialog(frmRejustify, "Login failed");
		}
	}

	/**
	 * Append text to the Log
	 * 
	 * @param text
	 *            the text to be appended
	 */
	private void updateTextArea(final String text) {
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				txtLog.append(text);
			}
		});
	}

	/**
	 * Redirect output streams to Log tab
	 */
	private void redirectSystemStreams() {
		OutputStream out = new OutputStream() {
			@Override
			public void write(int b) throws IOException {
				updateTextArea(String.valueOf((char) b));
				outStream.write(b);
			}

			@Override
			public void write(byte[] b, int off, int len) throws IOException {
				updateTextArea(new String(b, off, len));
				outStream.write(b, off, len);
			}

			@Override
			public void write(byte[] b) throws IOException {
				write(b, 0, b.length);
				outStream.write(b);
			}
		};

		System.setOut(new PrintStream(out, true));
		System.setErr(new PrintStream(out, true));
	}

	@Override
	public void newTrack(Track track) {
		if (getRowByTrack(track) == -1) {
			int length = track.getDuration();
			String duration = String.format("%02d:%02d",
					(length % 3600000) / 60000,
					Math.round((length % 60000) / 1000d));
			TableModel.addRow(new Object[] { track.getTitle(),
					track.getArtist(), track.getAlbum(), duration, "Queued", 0,
					track.getId() });
			tracks.put(track.getId(), track);
		}
		start();
	}

	@Override
	public void progressChanged(Track track) {
		int row = getRowByTrack(track);
		if (row >= 0) {
			TableModel.setValueAt(track.getProgress(), row, COLUMN_PROGRESS);
			TableModel.setValueAt("Downloading", row, COLUMN_STATUS);
		}
	}

	@Override
	public void error(Track track) {
		int row = getRowByTrack(track);
		if (row >= 0) {
			TableModel.setValueAt("ERROR", row, COLUMN_PROGRESS);
		}
		downloading = false;
		start();
	}

	/**
	 * Gets the row index from the {@link Track}
	 * 
	 * @param track
	 * @return the row index of the given track, -1 if not found
	 */
	private int getRowByTrack(Track track) {
		for (int i = 0; i < TableModel.getRowCount(); i++) {
			String id = (String) TableModel.getValueAt(i, COLUMN_ID);
			if (track.getId().equals(id)) {
				return i;
			}
		}
		return -1;
	}

	@Override
	public void trackFinished(Track track) {
		int row = getRowByTrack(track);
		if (row >= 0) {
			switch (track.getProgress()) {
			case -2:
				TableModel.setValueAt(100, row, COLUMN_PROGRESS);
				TableModel.setValueAt("Skipped", row, COLUMN_STATUS);
				break;
			case -1:
				TableModel.setValueAt(0, row, COLUMN_PROGRESS);
				TableModel.setValueAt("Stopped", row, COLUMN_STATUS);
				break;

			default:
				TableModel.setValueAt(100, row, COLUMN_PROGRESS);
				TableModel.setValueAt("Finished", row, COLUMN_STATUS);
				break;
			}
		}
		downloading = false;
		start();
	}

	/**
	 * Get a specific setting
	 * 
	 * @param key
	 * @return the value of the setting, returns default values if key not found
	 */
	private String getSetting(PreferenceKeys key) {
		String value = preferences.getValue(key.toString());
		if (value == null) {
			// Load Defaults
			switch (key) {
			case MONITOR_CLIPBOARD:
				value = "true";
				break;
			case OUTPUT_FOLDER:
				value = new File("").getAbsolutePath();
				break;
			case PASSWORD:
				value = "";
				break;
			case USERNAME:
				value = "";
				break;
			case SKIP_EXISTING:
				value = "true";
				break;
			case NAMING:
				value = "%num% %artist% - %title%";
				break;
			default:
				value = "";
				break;
			}
		}
		return value;
	}

	/**
	 * set value of a setting
	 * 
	 * @param key
	 * @param value
	 *            the new value
	 */
	private void setSetting(PreferenceKeys key, String value) {
		preferences.setValue(key.toString(), value);
	}

	private enum PreferenceKeys {
		USERNAME, PASSWORD, OUTPUT_FOLDER, MONITOR_CLIPBOARD, OUTPUT_FORMAT, SKIP_EXISTING, CREATE_FOLDER, SHOW_LOG, NAMING
	}

	/**
	 * Deletes the selected rows
	 */
	private void deleteSelectedRows() {
		int[] selectedRows = table.getSelectedRows();
		for (int i = selectedRows.length - 1; i >= 0; i--) {
			String status = (String) TableModel.getValueAt(selectedRows[i],
					COLUMN_STATUS);
			if (status.equals("Downloading")) {
				continue;
			}
			TableModel.removeRow(selectedRows[i]);
		}
	}
}
