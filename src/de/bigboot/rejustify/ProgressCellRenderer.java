/*
    This file is part of ReJustify.

    ReJustify is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ReJustify is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ReJustify.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.bigboot.rejustify;

import java.awt.Component;
import java.util.HashMap;

import javax.swing.JProgressBar;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;


/**
 * {@link ProgressCellRenderer} with {@link JProgressBar} as Component
 * Keeps references of ProgressBars for better Performace
 *
 */
public class ProgressCellRenderer implements TableCellRenderer{
	private HashMap<Integer, JProgressBar> pbars = new HashMap<Integer, JProgressBar>();

	@Override
	public Component getTableCellRendererComponent(JTable table, Object value,
			boolean isSelected, boolean hasFocus, int row, int column) {
		JProgressBar bar = pbars.get(row);
		if(bar == null)
		{
			bar = new JProgressBar();
			pbars.put(row, bar);
		}
		bar.setValue((Integer) value);
		return bar;
	}

}
