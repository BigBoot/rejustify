package de.bigboot.rejustify;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Collections;

import javax.swing.JComponent;
import javax.swing.Timer;

/**
 * Draws a Speed Graph
 */
public class SpeedGraph extends JComponent {
	private static final long serialVersionUID = 1L;
	private ArrayList<Long> speeds = new ArrayList<Long>();
	private Timer timer;
	private Long currentSpeed = 0l;
	

	public Long getCurrentSpeed() {
		return currentSpeed;
	}

	public void setCurrentSpeed(Long currentSpeed) {
		this.currentSpeed = currentSpeed;
	}

	public SpeedGraph()
	{
		timer = new Timer(1000, new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				if(currentSpeed==0l && (speeds.size()==0 || Collections.max(speeds) == 0l))
				{
					return;
				}
				speeds.add(currentSpeed);
				while(speeds.size()>getWidth())
				{
					speeds.remove(0);
				}
				repaint();
			}
		});
		timer.start();
	}
	
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.setColor(getBackground());
		int width = getWidth();
		int height = getHeight();
		g.fillRect(0, 0, width, height);
		if(speeds.size()==0)
		{
			return;
		}
		long max = Collections.max(speeds);
		double d = max>0?getHeight()*0.7d/max:0d;
		g.setColor(getForeground());
		for(int i = speeds.size()-1; i >= 0; i--)
		{
			int value = (int) (speeds.get(i)*d);
			int x = width-speeds.size()+i;
			g.drawLine(x, height, x, height-value);
		}
		g.setColor(Color.black);
		int currentY = height-(int) (currentSpeed*d);
		g.drawLine(0, currentY, width, currentY);
		g.drawString(Utils.readableFileSize(currentSpeed), 4, currentY-1);
	}
	
	public void setDelay(int delay)
	{
		timer.setDelay(delay);
	}
	
	
}
