/*
    This file is part of ReJustify.

    ReJustify is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ReJustify is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ReJustify.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.bigboot.rejustify;

/**
 * Class to store information about a Track
 *
 */
public class Track {
	private int trackNumber;
	private int cdNumber;
	private String title;
	private String album;
	private String artist;
	private String id;
	private int progress;
	private int duration;
	private boolean hasPlaylist;
	private String playlist = "";
	
	
	/**
	 * Default Constructor
	 * 
	 * @param trackNumber the track number
	 * @param cdNumber the CD number
	 * @param title the title
	 * @param album the album name
	 * @param artist the artist name
	 * @param id the Spotify ID
	 * @param duration the duration in ms
	 */
	public Track(int trackNumber, int cdNumber, String title, String album,
			String artist, String id, int duration) {
		super();
		this.trackNumber = trackNumber;
		this.cdNumber = cdNumber;
		this.title = title;
		this.album = album;
		this.artist = artist;
		this.id = id;
		this.duration = duration;
	}
	
	/**
	 * Get the Spotify ID of the track
	 * 
	 * @return the Spotify ID
	 */
	public String getId() {
		return id;
	}
	/**
	 * Set the Spotify ID of the track
	 * 
	 * @param id the Spotify ID
	 */
	public void setId(String id) {
		this.id = id;
	}
	
	/**
	 * Get the album name of the track
	 * 
	 * @return the album name
	 */
	public String getAlbum() {
		return album;
	}
	/**
	 * Set the album name of the track
	 * 
	 * @param album the album name
	 */
	public void setAlbum(String album) {
		this.album = album;
	}
	
	/**
	 * Get the artist name of the track
	 * 
	 * @return the artist name
	 */
	public String getArtist() {
		return artist;
	}
	/**
	 * Set the artist name of the track
	 * 
	 * @param artist the artist name
	 */
	public void setArtist(String artist) {
		this.artist = artist;
	}
	
	/**
	 * Get the track number of the track
	 * 
	 * @return the track number
	 */
	public int getTrackNumber() {
		return trackNumber;
	}
	/**
	 * Set the track numbe of the track
	 * 
	 * @param trackNumber the track numbe
	 */
	public void setTrackNumber(int trackNumber) {
		this.trackNumber = trackNumber;
	}
	
	/**
	 * Get the CD number of the track
	 * 
	 * @return the CD number
	 */
	public int getCdNumber() {
		return cdNumber;
	}
	/**
	 * Set the CD number of the track
	 * 
	 * @param cdNumber the CD number
	 */
	public void setCdNumber(int cdNumber) {
		this.cdNumber = cdNumber;
	}
	
	/**
	 * Get the title of the track
	 * 
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}
	/**
	 * Set the title of the track
	 * 
	 * @param title the title
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	
	/**
	 * Get the duration of the track
	 * 
	 * @return the duration in ms
	 */
	public int getDuration() {
		return duration;
	}
	/**
	 * Set the duration of the track
	 * 
	 * @param duration the duration in ms
	 */
	public void setDuration(int duration) {
		this.duration = duration;
	}
	
	/**
	 * Get the progress of the track
	 * 
	 * @return the progress, value between -1 and 100
	 * -1 means error
	 */
	public int getProgress() {
		return progress;
	}
	/**
	 * Set the progress of the track
	 * 
	 * @param progress the progress
	 */
	public void setProgress(int progress) {
		this.progress = progress;
	}
	
	/**
	 * Get if the track is part of a playlist
	 * 
	 * @return true if tack is part of a playlit, false otherwise
	 */
	public boolean isPlaylist()
	{
		return hasPlaylist;
	}	
	/**
	 * Get the playlist of the track
	 * 
	 * @return the name of the playlist
	 */
	public String getPlaylist()
	{
		return playlist;
	}
	/**
	 * Set the playlist of this track
	 * 
	 * @param playlist the playlist of the track, null for no playlist
	 */
	public void setPlaylist(String playlist) {
		this.hasPlaylist = playlist==null?false:true;
		this.playlist = playlist;
	}
}
