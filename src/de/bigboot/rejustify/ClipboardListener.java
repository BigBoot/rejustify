/*
    This file is part of ReJustify.

    ReJustify is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ReJustify is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ReJustify.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.bigboot.rejustify;

/**
 * Listener for clipboard changes.
 * Use with {@link ClipboardMonitor}
 *
 */
public interface ClipboardListener {
	/**
	 * Fired after the content of the clipboard changed
	 * @param value the new text in the clipboard
	 */
	public void clipboardChanged(String value);
}
