/*
    This file is part of ReJustify.

    ReJustify is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ReJustify is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ReJustify.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.bigboot.rejustify;

/**
 * Listener for Receiving Events sent by {@link Justifyx}.
 */
public interface ReJustifyListener {
	/**
	 * Fired when a {@link Track} was found while scanning a Spotify URL
	 * 
	 * @param track the new {@link Track}
	 */
	public void newTrack(Track track);
	
	/**
	 * Fired while downloading when the download progress changed
	 * 
	 * @param track the {@link Track} which is downloaded
	 */
	public void progressChanged(Track track);
	
	/**
	 * Fired when downloading a {@link Track} finished
	 * 
	 * @param track the {@link Track} which was downloaded
	 */
	public void trackFinished(Track track);
	
	/**
	 * Fired when an error occured while downloading a {@link Track}
	 * @param track the {@link Track} where the error occured
	 */
	public void error(Track track);
}
