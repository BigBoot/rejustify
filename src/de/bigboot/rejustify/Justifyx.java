/*
    This file is part of ReJustify.

    ReJustify is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    ReJustify is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with ReJustify.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.bigboot.rejustify;

import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.FileImageOutputStream;
import javax.swing.SwingWorker;
import javax.swing.Timer;

import com.klaxnek.justifyx.Base64Coder;
import com.klaxnek.justifyx.ImageFormats;
import com.klaxnek.justifyx.JustifyxException;

import de.felixbruns.jotify.JotifyConnection;
import de.felixbruns.jotify.exceptions.AuthenticationException;
import de.felixbruns.jotify.exceptions.ConnectionException;
import de.felixbruns.jotify.media.Album;
import de.felixbruns.jotify.media.Link;
import de.felixbruns.jotify.media.Playlist;
import de.felixbruns.jotify.media.Track;
import de.felixbruns.jotify.media.User;
import de.felixbruns.jotify.media.Link.InvalidSpotifyURIException;
import de.felixbruns.jotify.player.SpotifyInputStream;

import adamb.vorbis.CommentField;
import adamb.vorbis.VorbisCommentHeader;
import adamb.vorbis.VorbisIO;

/**
 * Finds and downloads tracks from Spotify
 */
public class Justifyx extends JotifyConnection {
	private static int substreamsize = 320 * 1024 * 30 / 8;
	private Integer discindex = 1;
	private Integer oldtracknumber = 1;
	private String country;
	private String formataudio = "ogg_320";
	protected String toplist_type = "track";
	protected String toplist_region;
	protected String oggcover = "old";
	protected long TIMEOUT = 20;
	protected int chunksize = 4096;
	protected boolean clean = false;
	private ArrayList<ReJustifyListener> listeners = new ArrayList<ReJustifyListener>();
	private DownloadWorker downloadWorker = null;
	private String outputFolder;
	private long bytesLoaded = 0;
	private long speed = 0;
	private boolean skipExisitingTracks = false;
	private boolean createPlaylistFolder = false;
	private boolean noFolders = false;
	private String pattern = "%num% %artist% - %title%";

	public String getPattern() {
		return pattern;
	}

	public void setPattern(String pattern) {
		this.pattern = pattern;
	}

	public boolean isNoFolders() {
		return noFolders;
	}

	public void setNoFolders(boolean noFolders) {
		this.noFolders = noFolders;
	}

	public boolean isSkipExisitingTracks() {
		return skipExisitingTracks;
	}

	public void setSkipExisitingTracks(boolean skipExisitingTracks) {
		this.skipExisitingTracks = skipExisitingTracks;
	}

	private void addBytesLoaded(int count) {
		synchronized (this) {
			bytesLoaded += count;
		}
	}

	private void resetBytesLoaded() {
		synchronized (this) {
			bytesLoaded = 0;
		}
	}

	private long getBytesLoaded() {
		synchronized (this) {
			return new Long(bytesLoaded);
		}
	}

	/*
	 * Returns the current speed in bytes per second
	 */
	public long getSpeed() {
		return speed;
	}

	/**
	 * Get the current Audio Bitrate
	 * 
	 * @return "ogg_320", "ogg_160" or "ogg_96"
	 */
	public String getFormat() {
		return formataudio;
	}

	/**
	 * Set the Audio Bitrate. Will Fallback to "ogg_320" if invalid value is
	 * specified
	 * 
	 * @param formataudio
	 *            Possible Values are "ogg_320", "ogg_160" and "ogg_96".
	 */
	public void setFormat(String formataudio) {
		if (!formataudio.equals("ogg_320") && !formataudio.equals("ogg_160")
				&& !formataudio.equals("ogg_96")) {
			formataudio = "ogg_320";
		}
		this.formataudio = formataudio;
	}

	/**
	 * Add a {@link ReJustifyListener}
	 * 
	 * @param listener
	 *            to be added
	 */
	public void addListener(ReJustifyListener listener) {
		listeners.add(listener);
	}

	/**
	 * Remove a {@link ReJustifyListener}
	 * 
	 * @param listener
	 *            to be removed
	 */
	public void removeListener(ReJustifyListener listener) {
		listeners.remove(listener);
	}

	public boolean isCreatePlaylistFolder() {
		return createPlaylistFolder;
	}

	public void setCreatePlaylistFolder(boolean createPlaylistFolder) {
		this.createPlaylistFolder = createPlaylistFolder;
	}

	/**
	 * Start grabbing of a {@link Track} Found Tracks will be send to registered
	 * {@link ReJustifyListener}s
	 * 
	 * @param spotiURL
	 *            URL or URI to Album/Track/Playlist
	 */
	public void getTracks(String spotiURL) {
		for (String s : spotiURL.split(" ")) {
			GetTracksWorker worker = new GetTracksWorker(s, this);
			worker.execute();
		}
	}

	/**
	 * SwingWorker for finding {@link Track}s on Spotify
	 * 
	 * @author BigBoot
	 * 
	 */
	private class GetTracksWorker extends
			SwingWorker<Void, de.bigboot.rejustify.Track> {
		private String spotiURL;
		private Justifyx justifyx;

		public GetTracksWorker(String spotiURL, Justifyx justifyx) {
			super();
			this.spotiURL = spotiURL;
			this.justifyx = justifyx;
		}

		@Override
		protected void process(List<de.bigboot.rejustify.Track> chunks) {
			for (de.bigboot.rejustify.Track track : chunks) {
				for (ReJustifyListener listener : listeners) {
					if (listener == null) {
						continue;
					}
					listener.newTrack(track);
				}
			}
		}

		@Override
		protected Void doInBackground() throws Exception {
			try {
				User spotifyuser = user();
				country = spotifyuser.getCountry();
				System.out.println(spotifyuser);
				System.out.println();
				if (!spotifyuser.isPremium())
					throw new JustifyxException(
							"[ERROR] You must be a 'premium' user");

				if (toplist_region == null)
					toplist_region = country;

				try {
					Link uri = Link.create(spotiURL);

					// Download track command
					if (uri.isTrackLink()) {
						Track track = browseTrack(uri.getId());
						if (track == null)
							throw new JustifyxException(
									"[ERROR] Track not found");
						Album album = track.getAlbum();
						de.bigboot.rejustify.Track t = new de.bigboot.rejustify.Track(
								track.getTrackNumber(), 1, track.getTitle(),
								album.getName(), track.getArtist().getName(),
								track.getLink().asString(), track.getLength());
						publish(t);
					}

					// Download playlist command
					else if (uri.isPlaylistLink()) {
						Playlist playlist = playlist(uri.getId());
						if (playlist == null)
							throw new JustifyxException(
									"[ERROR] Playlist not found");
						System.out.println("Playlist: " + playlist.getName()
								+ " | Author: " + playlist.getAuthor()
								+ " | Tracks: " + playlist.getTracks().size());
						System.out.println();
						Integer indexplaylist = 1;
						for (Track track : playlist.getTracks()) {
							String link = track.getLink().asString();
							Track t = justifyx.browse(track);
							de.bigboot.rejustify.Track rt = new de.bigboot.rejustify.Track(
									indexplaylist, 1, t.getTitle(), t
											.getAlbum().getName(), t
											.getArtist().getName(), link,
									t.getLength());
							rt.setPlaylist(playlist.getName());
							publish(rt);
							indexplaylist++;
						}
						indexplaylist = 0;
					}

					// Download album command
					else if (uri.isAlbumLink()) {
						Album album = browseAlbum(uri.getId());
						if (album == null)
							throw new JustifyxException(
									"[ERROR] Album not found");
						System.out.println("Album: " + album.getName()
								+ " | Artist: " + album.getArtist().getName()
								+ " | Tracks: " + album.getTracks().size()
								+ " | Discs: " + album.getDiscs().size());
						System.out.println();
						for (Track track : album.getTracks()) {
							int trackNumber = track.getTrackNumber();
							String title = track.getTitle();
							String albumName = album.getName();
							String artistname = album.getArtist().getName();
							String link = track.getLink().asString();
							int length = track.getLength();
							de.bigboot.rejustify.Track t = new de.bigboot.rejustify.Track(
									trackNumber, 1, title, albumName,
									artistname, link, length);
							publish(t);
						}
					} else {
						throw new JustifyxException(
								"[ERROR] Track, album or playlist not specified");
					}
				} catch (InvalidSpotifyURIException urie) {
					throw new JustifyxException(
							"[ERROR] Spotify URI is not valid");
				}
			} catch (JustifyxException je) {
				System.err.println(je.getMessage());
				je.printStackTrace();
			} catch (TimeoutException te) {
				System.err.println(te.getMessage());
				te.printStackTrace();
			}
			return null;
		}

	}

	public Justifyx() {
		super(20, TimeUnit.SECONDS);
		TIMEOUT = 20;
		Timer timer = new Timer(1000, new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				long l = getBytesLoaded();
				resetBytesLoaded();
				speed = l;
			}
		});
		timer.start();
	}

	/**
	 * Login to Spotify
	 * 
	 * @param user
	 *            Spotify username
	 * @param password
	 *            Spotify password
	 * @return true if login was successfull, false otherwise
	 */
	public boolean loginUser(String user, String password) {
		try {
			login(user, password);
			return true;
		} catch (ConnectionException ce) {
			System.err.println("[ERROR] Error connecting the server");
		} catch (AuthenticationException ae) {
			System.err.println("[ERROR] User or password is not valid");
		}
		return false;
	}

	/**
	 * SwingWorker for Downloading from Spotify
	 * 
	 */
	private class DownloadWorker extends
			SwingWorker<Track, de.bigboot.rejustify.Track> {
		private Justifyx justifyx;
		private Track track;
		private de.bigboot.rejustify.Track reJustifyTrack;
		private String parent;
		private String bitrate;
		private String option;
		private int index;

		public DownloadWorker(Justifyx justifyx, Link uri, String parent,
				String bitrate, String option, int index,
				de.bigboot.rejustify.Track reJustifyTrack) {
			super();
			this.justifyx = justifyx;
			try {
				this.track = justifyx.browseTrack(uri.getId());
			} catch (TimeoutException e) {
				cancel(true);
			}
			this.parent = parent;
			this.bitrate = bitrate;
			this.option = option;
			this.index = index;
			this.reJustifyTrack = reJustifyTrack;
		}

		@Override
		protected Track doInBackground() throws Exception {
			downloadTrack(justifyx, track, parent, bitrate, option, index);
			return track;
		}

		@Override
		protected void process(List<de.bigboot.rejustify.Track> chunks) {
			for (de.bigboot.rejustify.Track event : chunks) {
				for (ReJustifyListener listener : listeners) {
					if (listener == null) {
						continue;
					}
					listener.progressChanged(event);
				}
			}
		}

		@Override
		protected void done() {
			downloadWorker = null;
			for (ReJustifyListener listener : listeners) {
				if (listener == null) {
					continue;
				}
				listener.trackFinished(reJustifyTrack);
			}
		}

		private void downloadCover(Image image, String parent)
				throws TimeoutException, IOException {
			Iterator<ImageWriter> iter = ImageIO
					.getImageWritersByFormatName("jpeg");
			ImageWriter writer = (ImageWriter) iter.next();
			ImageWriteParam iwp = writer.getDefaultWriteParam();
			iwp.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
			iwp.setCompressionQuality(1);

			java.io.File coverfile = new java.io.File(
					replaceSpecialChars(parent), "folder.jpg");
			if (coverfile.exists()) {
				return;
			}
			coverfile.getParentFile().mkdirs();
			FileImageOutputStream output = new FileImageOutputStream(coverfile);
			writer.setOutput(output);
			IIOImage iimage = new IIOImage((BufferedImage) image, null, null);
			writer.write(null, iimage, iwp);
			writer.dispose();
			System.out.println("[100%] Cover ... ok");
		}

		protected void downloadTrack(Justifyx justifyx, Track track,
				String parent, String bitrate, String option, Integer index)
				throws JustifyxException, TimeoutException {

			// Downloading an album, if the new track number is lower than the
			// previous downloaded song, it means we are in a new disc
			if (option.equals("album")) {
				if (track.getTrackNumber() < oldtracknumber) {
					discindex++;
					oldtracknumber = 1;
				} else
					oldtracknumber = track.getTrackNumber();
			}

			try {
				String filename = createFileName(reJustifyTrack) + ".ogg";
				java.io.File file;
				if (noFolders) {
					file = new java.io.File(replaceSpecialChars(parent),
							replaceSpecialChars(filename));
				} else if (createPlaylistFolder && reJustifyTrack.isPlaylist()) {
					file = new java.io.File(replaceSpecialChars(String.format(
							"%s/%s", parent, reJustifyTrack.getPlaylist())),
							replaceSpecialChars(filename));
				} else {
					file = new java.io.File(replaceSpecialChars(String.format(
							"%s/%s/%s/", parent, reJustifyTrack.getArtist(),
							reJustifyTrack.getAlbum())),
							replaceSpecialChars(filename));
				}

				// Create directory
				if (parent != null && !file.getParentFile().exists())
					file.getParentFile().mkdirs();

				if (option == "album") {
					try {
						downloadCover(justifyx.image(track.getCover()),
								file.getParent());
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
				// Check restrictions and parse alternative files checking their
				// restrictions
				boolean allowed = true;
				Integer nalternative = 0;
				Integer talternative = 0;

				for (int i = 0; i < track.getRestrictions().size(); i++) {
					if (track.getRestrictions().get(i).getForbidden() != null)
						if (track.getRestrictions().get(i).getForbidden()
								.contains(country) == true)
							allowed = false;

					if (track.getRestrictions().get(i).getAllowed() != null)
						if (track.getRestrictions().get(i).getAllowed()
								.contains(country) == false)
							allowed = false;
						else
							allowed = true;
				}

				if (!allowed) {
					for (Track pista : track.getAlternatives()) {
						nalternative++;
						if (pista.getRestrictions().get(0).getForbidden() != null)
							if (pista.getRestrictions().get(0).getForbidden()
									.contains(country))
								allowed = false;
							else {
								allowed = true;
								talternative++;
							}

						if (pista.getRestrictions().get(0).getAllowed() != null)
							if (pista.getRestrictions().get(0).getAllowed()
									.contains(country)) {
								allowed = true;
								talternative = nalternative;
							}
					}
				}

				if (track.getFiles().size() == 0) {
					System.out.println("-- ko!");
					return;
				}

				if (allowed && nalternative == 0)
					download(track, file, bitrate);
				else if (allowed && nalternative > 0)
					download(track.getAlternatives().get(talternative - 1),
							file, bitrate);
				else {
					System.out.println("-- ko!");
					return;
				}

				if (isCancelled()) {
					return;
				}

				if (!clean)
					try {
						VorbisCommentHeader comments = new VorbisCommentHeader();

						// Embeds cover in .ogg for tracks and playlists (not
						// albums)
						if ((option.equals("track") || option
								.equals("playlist"))
								&& (!oggcover.equals("none"))) {
							byte[] imagedata = null;
							try {
								BufferedImage image = (BufferedImage) image(track
										.getCover());
								ByteArrayOutputStream output = new ByteArrayOutputStream();
								ImageIO.write(
										image,
										ImageFormats
												.getFormatForMimeType(ImageFormats.MIME_TYPE_JPG),
										new DataOutputStream(output));
								imagedata = output.toByteArray();
							} catch (Exception e) {
								reJustifyTrack.setProgress(-1);
								e.printStackTrace();
							}
							if (imagedata != null) {
								char[] testdata = Base64Coder.encode(imagedata);
								String base64image = new String(testdata);
								// doc: embedded artwork vorbis standards:
								// http://wiki.xiph.org/VorbisComment#Cover_art
								if (oggcover.equals("old")) {
									comments.fields.add(new CommentField(
											"COVERART", base64image));
									comments.fields.add(new CommentField(
											"COVERARTMIME",
											ImageFormats.MIME_TYPE_JPG));
								} else if (oggcover.equals("new")) {
									comments.fields.add(new CommentField(
											"METADATA_BLOCK_PICTURE",
											base64image));
								}
							}
						}

						comments.fields.add(new CommentField("ARTIST", track
								.getArtist().getName()));
						comments.fields.add(new CommentField("ALBUM ARTIST",
								track.getAlbum().getArtist().getName()));
						comments.fields.add(new CommentField("ALBUM", track
								.getAlbum().getName()));
						comments.fields.add(new CommentField("TITLE", track
								.getTitle()));
						comments.fields.add(new CommentField("DATE", String
								.valueOf(track.getYear())));

						// Sets track_number to real track number except in
						// playlists
						if (option.equals("playlist"))
							comments.fields.add(new CommentField("TRACKNUMBER",
									index.toString()));
						else
							comments.fields.add(new CommentField("TRACKNUMBER",
									String.valueOf(track.getTrackNumber())));

						// Sets disc_number and total_discs only when
						// downloading an
						// album
						if (option.equals("album")) {
							comments.fields.add(new CommentField("DISCNUMBER",
									discindex.toString()));
							comments.fields.add(new CommentField("TOTALDISCS",
									String.valueOf(track.getAlbum().getDiscs()
											.size())));
						}

						VorbisIO.writeComments(file, comments);
					} catch (IOException e) {
						reJustifyTrack.setProgress(-1);
						e.printStackTrace();
					}

				System.out.println(" ok");
			} catch (FileNotFoundException fnfe) {
				reJustifyTrack.setProgress(-1);
				fnfe.printStackTrace();
			} catch (IOException ioe) {
				reJustifyTrack.setProgress(-1);
				ioe.printStackTrace();
			}
		}

		protected void download(Track track, java.io.File file, String bitrate)
				throws TimeoutException, IOException {
			if (skipExisitingTracks && file.exists()) {
				reJustifyTrack.setProgress(-2);
				System.out.println("Skipping exising track");
				return;
			}
			if (track.getFiles().size() == 0)
				return;
			FileOutputStream fos = new FileOutputStream(file);
			SpotifyInputStream sis = new SpotifyInputStream(protocol, track,
					bitrate, chunksize, Justifyx.substreamsize);

			System.out.print(".");

			int counter = 0;
			int curPos = 168;
			byte[] buf = new byte[8192];
			sis.read(buf, 0, 167); // Skip Spotify OGG Header

			int size = -1;
			try {
				Field f = sis.getClass().getDeclaredField("streamLength");
				f.setAccessible(true);
				size = f.getInt(sis);
			} catch (NoSuchFieldException e) {
			} catch (SecurityException e) {
			} catch (IllegalArgumentException e) {
			} catch (IllegalAccessException e) {
			}

			while (true) {
				if (isCancelled()) {
					sis.close();
					fos.close();
					file.delete();
					System.out.print("Aborted");
					reJustifyTrack.setProgress(-1);
					return;
				}
				counter++;
				int length = sis.read(buf);
				curPos += length;
				addBytesLoaded(length);
				reJustifyTrack.setProgress(curPos * 100 / size);
				publish(reJustifyTrack);
				if (length < 0)
					break;
				fos.write(buf, 0, length);
				if (counter == 256) {
					counter = 0;
					System.out.print(".");
				}
			}

			sis.close();
			fos.close();
		}

	}

	/**
	 * Start Downloading of Track
	 * 
	 * @param track
	 *            to be downloaded
	 */
	public void downloadTrack(de.bigboot.rejustify.Track track) {
		if (downloadWorker != null) {
			System.out.println("[ERROR] Parallel downloads not allowed");
			return;
		}
		try {
			Link uri = Link.create(track.getId());
			if (uri.isTrackLink()) {
				downloadWorker = new DownloadWorker(this, uri, outputFolder,
						formataudio, "album", track.getTrackNumber(), track);
				downloadWorker.execute();
			}
		} catch (InvalidSpotifyURIException e) {
		}
	}

	/**
	 * Get the current output folder
	 * 
	 * @return the output folder
	 */
	public String getOutputFolder() {
		return outputFolder;
	}

	/**
	 * Sets the Output folder Be sure the Path is valid or there will be errors
	 * 
	 * @param outputFolder
	 *            the output folder, null will use current working dir
	 */
	public void setOutputFolder(String outputFolder) {
		if (outputFolder == null) {
			outputFolder = System.getProperty("user.dir");
		}
		this.outputFolder = outputFolder;
	}

	/**
	 * Cancel current Download
	 */
	public void cancelDownload() {
		if (downloadWorker == null) {
			System.err.println("[ERROR] Nothin to cancel");
			return;
		}
		downloadWorker.cancel(true);
	}

	public static boolean isWindows() {
		String os = System.getProperty("os.name").toLowerCase();
		return (os.indexOf("win") >= 0);
	}

	public static String replaceSpecialChars(String nombre) {
		if (nombre == null)
			return null;
		String drive = "";
		boolean isWindowsPath = false;
		if (isWindows()) {
			String s = nombre.substring(1, 3);
			nombre = nombre.replaceAll("\\\\", "/");
			if (s.equals(":\\")) {
				isWindowsPath = true;
				drive = nombre.substring(0, 1);
				nombre = nombre.substring(2);
			}
		}

		nombre = nombre.replaceAll("[\\\\:*?\"<>|\\$]", "_");
		if (isWindowsPath) {
			nombre = drive + ":" + nombre;
		}

		return nombre;
	}

	public static String capitalize(String s) {
		return s.substring(0, 1).toUpperCase() + s.substring(1).toLowerCase();
	}

	public String createFileName(de.bigboot.rejustify.Track track) {
		String title = pattern;
		title = title.replaceAll("%num%",
				String.valueOf(track.getTrackNumber()));
		title = title
				.replaceAll("%cdnum%", String.valueOf(track.getCdNumber()));
		title = title.replaceAll("%album%", track.getAlbum());
		title = title.replaceAll("%artist%", track.getArtist());
		title = title.replaceAll("%playlist%", track.getPlaylist());
		title = title.replaceAll("%duration%", String.format("%02d:%02d",
				(track.getDuration() % 3600000) / 60000,
				Math.round((track.getDuration() % 60000) / 1000d)));
		title = title.replaceAll("%title%", track.getTitle());
		title = title.replaceAll("%spotid%", track.getId());
		return title;
	}
}
